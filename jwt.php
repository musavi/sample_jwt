<?php
    $url = 'https://api.posteketab.com/api/v2/public/order/post/register';
    $token= "jwt eyJ0eXAiOiJ...";
    $shopId= "7638d3fc-1a00-448c-9724-1a7254c7ed9a";

    $state= 15;
    $city= 561;
    $address= "انتهای خیابان دانشگاه، مجتمع اداری دانشگاه علوم پزشکی اردبیل";
    $postalCode= 5618985991;
    $employee_code= 833;

    $data1 = [
        'shop_id' => $shopId,
        'address' => $address,
        'city_code' => $city,
        'province_code' => $state,
        'description' => null,
        'email' => null,
        'employee_code' => $employee_code,
        'first_name' => 'first_name',
        'last_name' => 'last_name',
        'mobile' => '09128525717',
        'phone' => null,
        'postal_code' => $postalCode,
        'pay_type' => 1,
        'order_type' => 0,
        'package_weight' => 10,
        'count' => 1,
        'discount' => 0,
        'price' => 100000,
        'title' => 'نام محصول',
        'weight' => 300,
        'product_id' => null
    ];
    $data2 = [
        'shop_id' => $shopId,
        'address' => $address,
        'city_code' => $city,
        'province_code' => $state,
        'description' => null,
        'email' => null,
        'employee_code' => $employee_code,
        'first_name' => 'first_name',
        'last_name' => 'last_name',
        'mobile' => '09128525717',
        'phone' => null,
        'postal_code' => $postalCode,
        'pay_type' => 1,
        'order_type' => 0,
        'package_weight' => 10,
        'products' => [
            'count' => 1,
            'discount' => 0,
            'price' => 100000,
            'title' => 'نام محصول',
            'weight' => 500,
            'product_id' => null
        ]
    ];

    $options = array(
        'http' => array(
            'header' => array(
                "Content-type: application/x-www-form-urlencoded",
                "Authorization: " . $token
            ),
            'method'  => 'POST',
            'content' => http_build_query($data1)
        )
    );
    $context  = stream_context_create($options);
    $result = file_get_contents($url, false, $context);
    if ($result === FALSE) { /* Handle error */ }

    var_dump(json_decode($result, true));
